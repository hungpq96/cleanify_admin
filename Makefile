run:
	python server/manage.py runserver

migrate:
	python server/manage.py makemigrations
	python server/manage.py migrate
